# Boondock

_Version 0.0.1_

A docker image with [boon](https://github.com/camchenry/boon) for automating builds of Love2D games in a CI system such as Bitbucket Pipelines, or CircleCI.

## Features

Based on the Bitbucket Pipelines docker image: `atlassian/default-image:2`.
Boon requires OpenSSL 1.1.1 or higher which isn't available for Ubuntu, so this image makes and installs it.
Downloads version 11.2 of Love2D.

## Usage

Run the following command from the folder that `main.lua` is in

    docker run -v "$(pwd)":/app -w /app cenetex/boondock build .
